
const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    fontFamily: {
      sans: ['Montserrat'],
      raleway: ['Raleway'],
      Mitr: ['Mitr']
     },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      blue: colors.blue,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      deeppurple: {
        light: '#B36EDA',
        DEFAULT: '#752A9F',
        dark: '#752A9F',
      },
      naranja:{
        light: '#ED8450',
        DEFAULT: '#FF671C',
        dark: '#FF671C'
      },
      amarillo:{
        light: '#F3AD43',
        DEFAULT: '#FFC900',
        dark: '#459315'
      },
      azul:{
        light: '#CDEAED',
        DEFAULT: '#75C3CB',
        dark: '#00BDCF'
      },
      gris:{
        light: '#F48967',
        DEFAULT: '#F5F5F5',
        dark: '#E51F12'
      }
    },
    minHeight: {
       '0': '0',
       '1/4': '25%',
       '1/2': '50%',
       '3/4': '75%',
       'full': '100%',
    },
    maxWidth: {
      '1/4': '25%',
      '1/2': '50%',
      '3/4': '75%',
    },
    inset:{
      '1/2': '49%'
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
