const jsrsasign = require('jsrsasign');
const isEqual = require('lodash/isEqual')
const uuidv4 = require("uuid/v4")

export default {
    ParseCertificate: function(arrayBuffer) {
        try {
            var x = readCertHex(jsrsasign.ArrayBuffertohex(arrayBuffer));
            return x;
        }
        catch(err) {
            throw new Error('ERR_UNABLE_TO_READ_CERTIFICATE')
        }
    },
    LoadSigner: function(certificate, privateKeyArrayBuffer, privateKeyPassword) {
        var rsaKey = decodePrivateKey(privateKeyArrayBuffer, privateKeyPassword)
        ensureMatch(certificate.x509.getPublicKey(), rsaKey);
        ensureNotExpired(certificate.x509);
        return {
            Sign: function(messageHash) {
                ensureSha256HashFormat(messageHash); 
                var sigFromHash = rsaKey.signWithMessageHash(messageHash, "sha256");
                var signingTime = new Date();
                var signatureId = uuidv4();
                return buildResults(signatureId, signingTime, messageHash, sigFromHash, certificate)
            }
        }
    }
}

function ensureSha256HashFormat(sha256HexString) {
    if (jsrsasign.hextorstr(sha256HexString).length != 32) {
        throw new Error('ERR_MESSAGE_HASH_TO_SIGN_MUST_BE_SHA256');
    }
}

function ensureMatch(publicKey, privateKey) {
    if (! isEqual(publicKey.n, privateKey.n) ) {
        throw new Error('ERR_PUBLIC_AND_PRIVATE_KEYS_DONT_MATCH');
    }
}

function ensureNotExpired(x509) {
    if(jsrsasign.zulutodate(x509.getNotAfter()) < new Date()) {
       throw new Error('ERR_CERTIFICATE_EXPIRED');
    }
}

function decodePrivateKey(privateKeyArrayBuffer, privateKeyPassword) {
    try
    {
        var privateKeyHex = jsrsasign.ArrayBuffertohex(privateKeyArrayBuffer);
        var privateKeyPem = jsrsasign.hextopem(privateKeyHex, 'ENCRYPTED PRIVATE KEY');
        var privateKey = jsrsasign.KEYUTIL.getKeyFromEncryptedPKCS8PEM(privateKeyPem, privateKeyPassword);
        return privateKey;
    }
    catch(err)
    {
        throw new Error('ERR_INVALID_PRIVATE_KEY_OR_PASSWORD')
    }
}

function readCertHex(certHex) {
    var x509 = new jsrsasign.X509();
    x509.readCertHex(certHex);
    return {
        pem: jsrsasign.hextopem(certHex, "CERTIFICATE"),
        summary: extractSummary(x509),
        x509: x509,
    }
}

function extractSummary(x509) {

    var subjectName = x509.getSubject().array.map(item => item[0]).find(item => item.type == 'CN').value;
    var issuerName = x509.getIssuer().array.map(item => item[0]).find(item => item.type == 'O').value;
    var rfc = x509.getSubject().array.map(item => item[0]).find(item => item.type == 'uniqueIdentifier').value;
    var satSerial = Array.from(x509.getSerialNumberHex()).filter((_, i) => {return (i % 2)}).join('');

    return {
            subject: {label: "Propietario", value: subjectName},
            rfc: {label: "RFC", value: rfc},
            serial: {label: "Número de Serie", value: satSerial},
            issuer: {label: "Emitido por", value: issuerName},
            notBefore: {label: "Inicio de Vigencia", value: jsrsasign.zulutodate(x509.getNotBefore())},
            notAfter: {label: "Fin de Vigencia", value: jsrsasign.zulutodate(x509.getNotAfter())}
    }
}

function buildResults(signatureId, signingTime, messageSha256Hash, signatureHex, certificate) {
    return {
        signatureId: signatureId,
        signingTime: signingTime,
        signatureMethod: {
            algorithm: "rsa_pkcs1_sha256"
        },
        signedDocument: {
            digestMethod: {
                algorithm: "sha256"
            },
            digestValue: {
                hex: messageSha256Hash, 
                base64: jsrsasign.hextob64(messageSha256Hash),
            }
        },
        signatureValue: {
            base64: jsrsasign.hextob64(signatureHex)
        },
        certificate: {
            summary: certificate.summary,
            derEncoded: {
                base64: jsrsasign.hextob64(certificate.x509.hex)
            },
            dump: {
                x509: certificate.x509.getParam()
            }
        }
    }
}