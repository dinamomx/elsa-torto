/*
    Hamburger menu in mobile.
*/ 

const boton = document.querySelector('#boton');
const menu = document.querySelector('#menu');


boton.addEventListener('click', () => {
    console.log('click')
    menu.classList.toggle('hidden');
})

 
menu.classList.toggle('hidden');


oculta.addEventListener('click', () => {
  menu.classList.toggle('hidden')
})
oculta2.addEventListener('click', () => {
  menu.classList.toggle('hidden')
})
oculta3.addEventListener('click', () => {
  menu.classList.toggle('hidden')
})
oculta4.addEventListener('click', () => {
  menu.classList.toggle('hidden')
})



theButton.onclick = function () { 

        var x = document.getElementById("myDIV");
        var x1 = document.getElementById("myDIV1");
        if (x.style.display === "none") {
          x.style.display = "block";
          x1.style.display = "none";
          document.getElementById("myDIV").innerHTML = '  <strong class="text-base font-bold">  ‘’Líder Asertivo, Equipo Productivo’’</strong> <br><br>  ¿Qué es lo primero que haces cuando algo no te late? ¿Preguntas o críticas? Si es lo segundo, tal vez este taller virtual sea la mejor inversión para ti.  Orientado a mejorar la comunicación que tienes con tu equipo de trabajo.<br><br>   <strong class="text-base font-bold">  Orientado a: </strong> <br><br>  Profesionales independientes, Directivos, Gerentes y Coordinadores de Organizaciones públicas y privadas, Directores de Micropymes o Pymes, Directores y Dueños de empresas familiares y Emprendedores: grupo multidisciplinario, edades distintas, ambos sexos.Orientado a: Profesionales independientes, Directivos, Gerentes y Coordinadores de Organizaciones públicas y privadas, Directores de Micropymes o Pymes, Directores y Dueños de empresas familiares y Emprendedores: grupo multidisciplinario, edades distintas, ambos sexos. <br><br> <strong>Objetivos:</strong> <ul><li>Desarrolla y optimiza tus habilidades de comunicación en simultáneo a transformar positiva y visiblemente tu conducta.</li><li>Disminuye tu mecanicidad en la cotidianidad.</li><li>Relaciónate con tu propio Ser y con los demás con una mayor calidad en los entornos laboral, familiar y social.</li>Resignifica tu Autoconcepto forjando una mejor versión de tí.<li></li></ul> <br> <strong>Inicio:</strong> 07 de Marzo de 7pm a 8pm <br> <strong>Duración:</strong> 16 hrs.<br> <strong>Costo:</strong> $3,600 + IVA<br> Opción de 3 pagos parcializados de $1,300 + IVA <br> Deducible fiscalmente al 100% <br><br>   <a   id="theButton" class="cursor-pointer transition ease-in duration-300 inline-block text-sm px-4 py-2 leading-none border rounded-full text-white border-azul bg-azul hover:bg-azul-dark hover:border-transparent hover:text-white  mt-4 lg:mt-0" href="#contacto">Inscribete</a> <br><br>';
        } else {
          x.style.display = "none";
          x1.style.display = "block";
        }
      
  
    
  
}



theButton2.onclick = function () { 

    var x = document.getElementById("myDIV_1");
    var x1 = document.getElementById("myDIV_0");
    if (x.style.display === "none") {
      x.style.display = "block";
      x1.style.display = "none";
      document.getElementById("myDIV_1").innerHTML = '<strong> ‘’Erotismo y Castidad’’</strong> <br><br>El taller Erotismo y castidad es una invitación a explorar la sexualidad humana de manera sensible, respetuosa y abierta para llegar a las profundidades del placer mediante la excitabilidad.<br><br> El erotismo y la castidad son dos vías paralelas, alternas y complementarias para el desarrollo de la conciencia. Ofrece horizontes para ejercer amorosamente: <br><br><ul><li>- Erotismo sin miedos, prejuicios ni ignorancia.</li><li>- Castidad, para encontrar la plenitud de ser.</li></ul> <br> <strong> Orientado a: </strong> <br><br>Público general. Edades distintas, ambos sexos.  <br><br> <strong>Objetivos:</strong> <ul><li>Decodificar los condicionamientos educativos que la escuela, sociedad y familia han impuesto</li><li>Conocer a fondo la naturaleza de tu potencial sexual y sus funciones primordiales: conciencia, placer y procreación</li><li>Identificar el tipo de atracción que tienes hacia otras personas</li><li>Aprender a manejar tu energía sexual para que no traiga disfunciones a tu vida diaria </li><li>Conocer a fondo la naturaleza y profundidad del orgasmo masculino y femenino.</li><li>Descubrir el equilibrio entre Erotismo y Castidad en tu propia vida como rutas complementarias.</li></ul> <br> <strong>Inicio:</strong> 24 de Noviembre  <br> <strong>Duración:</strong> 16 hrs.<br> <strong>Costo:</strong> $3,600 + IVA<br> Opción de 3 pagos parcializados de $1,300 + IVA <br> Deducible fiscalmente al 100% <br><br>   <a   id="theButton" class="cursor-pointer transition ease-in duration-300 inline-block text-sm px-4 py-2 leading-none border rounded-full text-white border-azul bg-azul hover:bg-azul-dark hover:border-transparent hover:text-white  mt-4 lg:mt-0" href="#contacto">Inscribete</a> <br><br>';
    } else {
      x.style.display = "none";
      x1.style.display = "block";
    }
  



}

theButton3.onclick = function () { 

    var x = document.getElementById("myDIV_11");
    var x1 = document.getElementById("myDIV_00");
    if (x.style.display === "none") {
      x.style.display = "block";
      x1.style.display = "none";
      document.getElementById("myDIV_11").innerHTML = '<strong> ‘’Huella de Abandono’’</strong> <br><br> La huella de abandono produce una sensación de vacío y dos de las emociones más extremas: angustia existencial y desamparo. .<br><br> Es la causante de la dependencia afectiva que vives y de los vínculos tóxicos que transtornan tu paz. También es la causante del apego que tal vez tienes con tu pareja, tu mascota, el alcohol, el tabaco, los alimentos, entre otros.   <br><br>    <strong> Orientado a: </strong> <br><br>Público general. Edades distintas, ambos sexos. <br><br> <strong>Objetivos:</strong> <ul><li><li>Recibir las herramientas necesarias para superar heridas del pasado y mejorar todas tus relaciones.</li><li>Identificar tu huella de abandono para desmantelarla y liberarte a ti mismo de ti mismo.</li><li>Vivir tu segundo nacimiento.</li><li>Reconciliarte con tus padres.</li>Descubrir el perfil específico de tu huella de abandono. </li><li>Detectar apegos fundamentales a ejes de imantación: poder, sexo, dinero, fama.</li><li>Determinar el alcance de esta huella de abandono en tu proyecto de vida y cómo afecta en tus potenciales.</li></ul> <br> <strong>Inicio:</strong> 12 de Enero del 2021<br> <strong>Duración:</strong> 16 hrs.<br> <strong>Costo:</strong> $3,600 + IVA<br> Opción de 3 pagos parcializados de $1,300 + IVA <br> Deducible fiscalmente al 100% <br><br>   <a   id="theButton" class="cursor-pointer transition ease-in duration-300 inline-block text-sm px-4 py-2 leading-none border rounded-full text-white border-azul bg-azul hover:bg-azul-dark hover:border-transparent hover:text-white  mt-4 lg:mt-0" href="#contacto">Inscribete</a> <br><br>';
    } else {
      x.style.display = "none";
      x1.style.display = "block";
    }
  



}
