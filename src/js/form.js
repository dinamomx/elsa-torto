
  import Vue from '../../node_modules/vue/dist/vue.common.js';
import FormGeneric from '../../FormGeneric.vue';
import { CollapseTransition } from 'vue2-transitions';

Vue.component(FormGeneric.name, FormGeneric);
Vue.component(CollapseTransition.name, CollapseTransition);
//console.log(FormGeneric);


Document.prototype.ready = callback => {
  const stateIsReady = state => state === 'interactive' || state === 'complete';
  if (callback && typeof callback === 'function') {
    if (stateIsReady(document.readyState)) {
      return callback();
    }
    document.addEventListener('DOMContentLoaded', () => {
      if (stateIsReady(document.readyState)) {
        return callback();
      }
    });
  }
};
document.ready(() => {
  const app = new Vue({
    el: '#app',
    data: {
      showTerms: false,
      showModel: {
        sportage: false,
        sorento: false
      }
    }
  });

  window.$app = app;
});