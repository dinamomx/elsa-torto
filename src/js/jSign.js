import rccrypto from "./rccrypto.js";
// const rccrypto = require('rccrypto');

var jSign = (function () {
    let urls;
    let permissions;

    function SignDocVM() {
        let self = this;

        self.hasError = ko.observable(false);

        self.errorMessage = ko.observable('');

        self.errorMessage.subscribe(function (val) {
            self.hasError(val != '');
        }, this);

        self.selectedCertificate = null;

        self.selectedPrivateKey = null;

        self.signature = null;

        self.specifiedPassword = ko.observable('12345678a');

        self.messageHash = ko.observable('aa6adf613f1c3380074d2b9d9944c55418026466dd6620df8daff52a78e484a3'); //hola mundo este es un mensaje

        self.onSelectCertFile = function (file) {
            //console.log(file);
            /* Is the file an cert? */
            self.errorMessage('Debe elegir un archivo .cer');

            if (!file || !file.type.match(/cer.*/)) return;

            self.errorMessage('');

            var reader = new FileReader();
            reader.onload = () => {
                var certArrayBuffer = reader.result;
                try {
                    self.selectedCertificate = rccrypto.ParseCertificate(certArrayBuffer);
                    console.log("done: ParseCertificate");
                    console.log(self.selectedCertificate.summary);
                } catch (err) {
                    console.log(err);
                    self.errorMessage(err.message);
                }
            };

            reader.readAsArrayBuffer(file);
        };

        self.onSelectKeyFile = function (file) {
            console.log("key seleccionado");
            var reader = new FileReader();
            reader.onload = () => {
                var privateKeyArrayBuffer = reader.result;
                self.selectedPrivateKey = privateKeyArrayBuffer;

                console.log("Leido ahora");
            };

            reader.readAsArrayBuffer(file);
        };

        self.onSignDocument = function () {
            try {
                var signer = rccrypto.LoadSigner(
                    self.selectedCertificate,
                    self.selectedPrivateKey,
                    self.specifiedPassword()
                );

                console.log("firmado inicio");
                console.log(self.messageHash());
                self.ignature = signer.Sign(self.messageHash());
                console.log(self.ignature);
                console.log("firmado fin");

                //this.$emit("success", { signature: this.Signature });
            } catch (err) {
                //this.$emit("onError", { ErrorCode: err });
                console.log(err);
                self.errorMessage(err.message);
            }
        };
        // self.selectKey = function (file) {
        //     console.log(file);
        //     /* Is the file an image? */
        //     if (!file || !file.type.match(/cer.*/)) return;
        // };


        //self.errors = ko.observableArray([]);

        // self.vm = ko.mapping.fromJS(data);

        // self.vm.username.extend({required: true, insertMessages: false});
        // self.vm.clave.extend({required: true, insertMessages: false});

        // self.formIsValid = ko.observable(false);

        // self.onClickLogin = function () {

        // };
    };

    let init = function () {

        console.log("Inicializa...");
        let viewModel = new SignDocVM();
        let htmlElement = document.getElementById("sign-form");
        ko.applyBindings(viewModel, htmlElement);
    };

    return {
        init: init
    };
})();

document.addEventListener("DOMContentLoaded", function () {
    console.log("Aquí hay algo...");
    jSign.init();
});
